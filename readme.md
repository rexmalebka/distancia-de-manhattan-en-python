# distancia de Manhatan

Este archivo lo hice para calcular la distancia de Manhattan entre dos puntos y comprobar que cualquier trayectoria para llegar en la que el recorrido sea siempre positivo tanto en x como en y es la misma.

![ilustración de la distancia de Manthatan](distancia.png "Distancia de Manhattan")

Abordé el problema como un problema de vectores y coordenadas.
En el código implemento una función que devuelve algunas maneras de calcular la distancia de Manhattan.
Para comprobar el teorema utilicé sets de pythons que convierte una tupla en un conjunto con valores únicos y si teóricamente son los mismos valores todos, el tamaño de ese conjunto debería de ser 1 

[Distancia de Manhattan en wikipedia](https://es.wikipedia.org/wiki/Geometr%C3%ADa_del_taxista)

