def distancia( p1, p2 ):
    #p1 [ x, y ], p2 [ x. y ]
    #distancia linea roja
    dx = abs( p2[0] - p1[0] )
    dy = abs( p2[1] - p1[1] )
    dist1 = dx + dy

    #distancia linea azul
    #se tiene que ir hasta donde tope en y o en x
    if dx > dy:
        # si es un rectángulo más largo en x
        dist2 = 2 * dy
        dist2 += ( dx - dy )
    elif dy > dx:
        # si es un rectángulo más largo en y
        dist2 = 2 * dx
        dist2 += ( dy - dx )
    else:
        # si es un cuadraro
        dist2 = 2 * dx

    #distancia linea amarilla
    if dx > 1:
        dist3  = (dx - 1) + dy + 1
    elif dy > 1:
        dist3  = (dy - 1) + dx + 1

    #otra distancia
    #intenta pasar por la mitad  en x 
    centx = dx // 2
    dist4 = centx + dy + (dx - centx )


    return ( dist1, dist2, dist3, dist4)

##set crea un conjunto con valores unicos 
print( distancia( [0,0],  [3,3] ), len(set(distancia( [0,0],  [3,3] )))==1 )
print( distancia( [1,1],  [4,4] ), len(set(distancia( [1,1],  [4,4] )))==1 )
print( distancia( [0,0],  [4,0] ), len(set(distancia( [0,0],  [4,0] )))==1 )
print( distancia( [0,0],  [0,4] ), len(set(distancia( [0,0],  [0,4] )))==1 )
print( distancia( [0,0],  [5,3] ), len(set(distancia( [0,0],  [5,3] )))==1 )
print( distancia( [0,0],  [3,5] ), len(set(distancia( [0,0],  [3,5] )))==1 )
